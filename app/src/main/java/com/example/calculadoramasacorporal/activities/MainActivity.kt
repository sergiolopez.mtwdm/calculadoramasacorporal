package com.example.calculadoramasacorporal.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.calculadoramasacorporal.R
import com.example.calculadoramasacorporal.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
//    private lateinit var btnCalcular: Button
//    private lateinit var editAltura:EditText
//    private lateinit var editPeso:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

//        editAltura = findViewById(R.id.edit_altura)
//        editPeso = findViewById(R.id.edit_peso)
//        btnCalcular = findViewById(R.id.btn_calcular)
//
//        val altura = editAltura.text.toString()
//        val peso = editPeso.text.toString()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCalcular.setOnClickListener {
            var peso = binding.editPeso.text.toString().toDouble()
            var estatura = binding.editAltura.text.toString().toDouble()

            val pow = (estatura.pow(2))
            val imc: Double = peso / (estatura.pow(2))
            val imcstring = getString(R.string.imc)
            binding.txtImc.text = "$imcstring: $imc"
            when {
                imc <= 18.5 -> {
                    binding.txtDescripcion.text = getString(R.string.bajopeso)
                    Picasso.get().load(R.drawable.pesobajo).into(binding.imageView)
                }
                imc > 18.5 && imc < 25 -> {
                    binding.txtDescripcion.text = getString(R.string.normal)
                    Picasso.get().load(R.drawable.normal).into(binding.imageView)
                }
                imc >= 25.0 && imc < 30 -> {
                    binding.txtDescripcion.text = getString(R.string.sobrepeso)
                    Picasso.get().load(R.drawable.sobrepeso).into(binding.imageView)
                }
                imc >= 30.0 -> {
                    binding.txtDescripcion.text = getString(R.string.obesidad)
                    Picasso.get().load(R.drawable.obesidad).into(binding.imageView)
                }
                else -> {
                    binding.txtDescripcion.text = getString(R.string.nocalculado)
                }
            }
//            Toast.makeText(
//                this,
//                "Descripción: $descripcion Pow: $pow IMC: $imc - Estatura: $estatura - Peso: $peso",
//                Toast.LENGTH_SHORT
//            ).show()
        }
    }
}